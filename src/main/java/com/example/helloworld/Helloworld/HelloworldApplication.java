package com.example.helloworld.Helloworld;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.view.InternalResourceViewResolver;

@SpringBootApplication
public class HelloworldApplication {

	public static void main(String[] args) {
		SpringApplication.run(HelloworldApplication.class, args);
	}
	@Bean
	public ViewResolver configureViewResolver() {
	    InternalResourceViewResolver viewResolve = new InternalResourceViewResolver();
	    viewResolve.setPrefix("/WEB-INF/");
	    viewResolve.setSuffix(".jsp");

	    return viewResolve;

}}

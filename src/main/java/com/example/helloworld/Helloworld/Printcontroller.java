package com.example.helloworld.Helloworld;

import org.springframework.stereotype.Controller;


import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class Printcontroller{

	@RequestMapping("/intro")
	public String firstPage() {
		return "intro";
	}
	@RequestMapping("/print")
	public String secondPage() {
		return "print";
	}
}